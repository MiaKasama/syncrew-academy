<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>グラデーションJSP その2</title>
</head>
<body>
<% int add = 42; %>

<h1>グラデーションJSP その2</h1>
グラデーションの幅：<%= add %><p>

<table border="1">
  <tr>
<%
  int color;

//for文用変数
int i, j;

//表の行・列の数
int n;

//addでnの値を決定
n = 255/(add*2)+1;

//4×4の表を作る
//下に少しずつ白くなる
  for(i=0; i<n; i++){
    out.println("<tr>");
    color = i*add;
    
//右に少しずつ白くなる
    for (j=0; j<n; j++) {
      String colorHex = String.format("%02x", color);
                                         // colorを16進数(2ケタ)に変換
      String colorHTML = colorHex + colorHex + colorHex;
                                         // HTMLの色指定形式の作成
      out.println("    <td style=\"height: 50px; width: 50px; background-color: #"
          + colorHTML + ";\"><br></td>");
      color += add;
    }

    out.println("</tr>");
  }
%>
  </tr>
</table>
</body>
</html>