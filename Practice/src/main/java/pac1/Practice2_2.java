package pac1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practice2_2
 */
@WebServlet("/Practice2_2")
public class Practice2_2 extends HttpServlet {
	//private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	  
	  PrintWriter out = response.getWriter();
    out.println("<html>");
    
    //見出し
    out.println("<head>");
    out.println("<title>お天気予想</title>");
    out.println("</head>");
    
    //予想します
    out.println("<body>");
    out.println("<h1>お天気を予想します</h1>");
    
    //乱数randを使用
    double rand = Math.random();
    //0.5未満にて晴れ
    if (rand <= 0.5d) {
      out.println("天気は、晴れです。<p>");
      //0.8未満にて曇り
    } else if (rand <= 0.8d) {
      out.println("天気は、くもりですねぇ。<p>");
      //それ以上だと雨
    } else {
      out.println("天気は、雨でしょう。<p>");
    }
    out.println("</body>");
    out.println("</html>");
	}

}
