package pac1;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practice2_4
 */
@WebServlet("/Practice2_4")
public class Practice2_4 extends HttpServlet {
	//private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
  
  int targetNumber = 0; //正解の数は何か
  int times = 0; //挑戦回数
  
	@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	  //挑戦開始時に正解を決定
	  if (times == 0) {
      targetNumber = (int) (Math.random() * 9d) + 1;
    }
    times++;
    
    //数一致で当たり、そうでなければはずれ
    String Number_s = request.getParameter("number");
    int enteredNumber = Integer.parseInt(Number_s);

    String result = "はずれ…";
    if (enteredNumber == targetNumber) {
      result = "当たり！";
    }

    PrintWriter out = response.getWriter();
    out.println("<!DOCTYPE html>");
    out.println("<html>");
    //見出し
    out.println("<head>");
    out.println("<meta charset=\"UTF-8\"/>");
    out.println("<title>数当てゲーム結果</title>");
    out.println("</head>");
    
    //挑戦回数、選んだ数、結果表示
    out.println("<body>");
    out.println("<h1>数当てゲーム結果</h1>");
    out.println(times + "回目　あなたが選んだ数：" + enteredNumber);
    out.println("<br>");
    out.println("<br>");
    out.println("結果は、" + result);
    out.println("<br>");
    out.println("<br>");
    
    //もう一度やる際には元のページに戻る
    out.println("<a href=\"Practice2_4.html\">もう一度やる</a>");
    out.println("</body>");
    out.println("</html>");
    
    //正解したら挑戦回数リセット
    if (enteredNumber == targetNumber) {
      times = 0;
    }
	}

}
