package pac1;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practice2_5
 */
@WebServlet("/Practice2_5")
public class Practice2_5 extends HttpServlet {
	//private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
	  //htmlにて得た情報を取得(全てint型なので悪しからず)
		String strnumber1 = request.getParameter("number1");
    int number1 = Integer.parseInt(strnumber1);
    String strnumber2 = request.getParameter("number2");
    int number2 = Integer.parseInt(strnumber2);
    String calculate = request.getParameter("calculate");

    PrintWriter out = response.getWriter();
    out.println("<!DOCTYPE html>");
    out.println("<html>");
    //見出し
    out.println("<head>");
    out.println("<meta charset=\"UTF-8\"/>");
    out.println("<title>四則演算結果</title>");
    out.println("</head>");
    
    
    out.println("<body>");
    out.println("<h1>四則演算の結果</h1>");
    out.println("<table>");
    out.println("<tr><td>数字1つ目</td><td>　</td><td>数字2つ目</td><td>　</td><td>結果</td></tr>");
    
    out.println("<tr style=\"text-align: center\"><td>" + number1 + "</td><td>");
    
    //四則演算のどれを選んだかによって記号を変える
    switch (calculate) {
      case "addition": out.print("＋");break;
      case "subtraction": out.print("－");break;
      case "multiplication": out.print("×");break;
      case "division": out.print("÷");break;
    }
    
    out.println("</td><td>" + number2 + "</td><td>＝</td><td>");
    
    //四則演算のどれを選んだかによって計算結果を変える
    switch (calculate) {
      case "addition":out.print(number1 + number2);break;
      case "subtraction":out.print(number1 - number2);break;
      case "multiplication":out.print(number1 * number2);break;
      case "division":out.print(number1 / number2);break;
    }
    out.println("</td></tr>");
    out.println("</table>");
    out.println("</body>");
    out.println("</html>");
	}

}
