package pac1;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practice2_6_1
 */
@WebServlet("/Practice2_6_1")
public class Practice2_6_1 extends HttpServlet {
	//private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	  
    //プレイヤーの手
    String player = request.getParameter("hand");

    //コンピュータの手をランダムで決める
    double RSPcpu = Math.random() * 3;
    String cpu = "";
    if (RSPcpu <= 1.0d) {
      cpu = "rock";
    } else if (RSPcpu <= 2.0d) {
      cpu = "scissors";
    } else {
      cpu = "paper";
    }

    /**
     * 勝負の判定
     */
    String result = "";
    //あいこの処理
    if (cpu.equals(player)) {
      result = "あいこ";
      //プレイヤー勝利の処理
    } else if ((cpu.equals("rock")) && (player.equals("paper"))) {
      result = "Player";
    } else if ((cpu.equals("scissors")) && (player.equals("rock"))) {
      result = "Player";
    } else if ((cpu.equals("paper")) && (player.equals("scissors"))) {
      result = "Player";
    } else {
      //cpu勝利の処理
      result = "CPU";
    }
    //結果発表
    if (result.equals("あいこ")) {
      result = "引き分け";
    } else if (result.equals("Player")) {
      result = "あなたの勝ち";
    } else {
      result = "プログラムの勝ち";
    }

    PrintWriter out = response.getWriter();
    out.println("<!DOCTYPE html>");
    out.println("<html>");
    out.println("<head>");
    out.println("<meta charset=\"UTF-8\"/>");
    out.println("<title>じゃんけん結果</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<h1>じゃんけん結果</h1>");
    out.println("あなたが選んだ手：" + Judgment(player) + "<br>");
    out.println("プログラムが選んだ手：" + Judgment(cpu) + "<br>");
    out.println("勝負の結果は、" + result + "でした！");
    out.println("<br>");    
    out.println("<br>");      
    out.println("<a href=\"Practice2_6_1.html\">もう一度やる</a>");
    out.println("</body>");
    out.println("</html>");
  }
	
	//判定関数
  String Judgment(String hand) {
    switch (hand) {
    case "rock":
      return "グー";
    case "scissors":
      return "チョキ";
    default:
      return "パー";
    }
	}

}
