package pac1;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Practice2_3
 */
@WebServlet("/Practice2_3")
public class Practice2_3 extends HttpServlet {
	//private static final long serialVersionUID = 1L;
  
  //訪問者を数える用（初期値0）
  int accessCount = 0;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
				
		PrintWriter out = response.getWriter();
    out.println("<!DOCTYPE html>");
    out.println("<html>");
    //見出し
    out.println("<head>");
    out.println("<meta charset=\"UTF-8\"/>");
    out.println("<title>アクセスカウンタ</title>");
    out.println("</head>");
    
    //アクセス数
    //カウントを1増やす
    accessCount++;
    
    //表示
    out.println("<body>");
    out.println("<h1>アクセスカウンタ</h1>");
    out.println("あなたは、" + accessCount + "人目の訪問者です！");
    out.println("</body>");
    out.println("</html>");
	}

}
